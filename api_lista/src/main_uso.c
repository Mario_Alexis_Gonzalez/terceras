﻿/*
* Este programa sirve para verificar el funcionamiento del API "arreglo_lista.so"
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "arreglo_lista.h"

int main(int argc, char const *argv[]){
	
	printf("Para un arreglo Uni de tipo int\n\n");
	classLista *obj = new_ObjLista();//CREAR OBJETO DE CLASE LISTA
	classLista *ob2 = new_ObjLista();
	assert(obj != NULL && ob2 != NULL);//COMPROBAR QUE LA ESTRUCTURA ESTE CREADA 

	obj->setElementosListaInt(obj, 5);//Definir los elementos de la lista
	printf("\n\n");
	obj->printListaInt(obj);//imprimir una lista de tipo entero
	printf("\n");
	
	obj->setValorListaInt(obj, 0, 1);//imprimir los valores que contiene la lista 
	obj->setValorListaInt(obj, 1, 2);
	obj->setValorListaInt(obj, 2, 3);
	obj->setValorListaInt(obj, 3, 4);
	obj->setValorListaInt(obj, 4, 5);

	printf("Impresión de arreglo despues de meter valores....\n");
	obj->printListaInt(obj);
	printf("\n");
	
	printf("\tPrimera parte bien..\n\n");	

	ob2->setElementosListaFloat(ob2, 5);//Definir los elementos de la lista
	printf("\n");
	ob2->printListaFloat(ob2);//imprimir una lista de tipo flotante
	printf("\n");
	
	ob2->setValorListaFloat(ob2, 0, 1.54343);//imprimir los valores que contiene la lista 
	ob2->setValorListaFloat(ob2, 1, 2.74343);
	ob2->setValorListaFloat(ob2, 2, 3.83434);
	ob2->setValorListaFloat(ob2, 3, 4.84343);
	ob2->setValorListaFloat(ob2, 4, 5.83434);

	printf("ARREGLO CON VALORES\n");
	ob2->printListaFloat(ob2);

	printf("\tVA BIEN \n");
	free_ObjLista(obj);//vacía la estructura y la elimina
	free_ObjLista(ob2);//vacía la estructura y la elimina
	printf("\tFIN\n\n");
	return 0;
}
